//-----------------------------------------------------------------------
//
//  Some functions to reproduce a Landau distribution convoluted with a Gaussian
//   the Landau parameters correspond to straggling from MIP into silicon
//   using H. Bichsel Rev. Mod. Phys. , Vol. 60, No. 3, July 1988, p663
//
// Inputs needed for functions to call
//  thickness of silicon crossed (in cm)
//  noise (in e-)
//  conversion factor e- -> arbitrary unit (use 1 if you don't know)
//  signal fraction -> fraction of signal on the seed pixel
//
// Functions to call after loading (.L bichsel.C+):
//  bichsel() -> help
//  eloss(thickness, noise, conversion): draw Landau MPV and width evolution with thickness
//  effi(thickness, sigfraction, noise, conversion): draw expected efficiencies with threshold
//   
// Core internal functions (not for direct usage):
//  langaufun(...) -> Landau * gaussian function
//  mpvfun(...) & widthfun() -> MPV and width functions
//  raiofun(...) -> ratio of width over MPV
//  mpvpmicfun(...) -> MPV per micron
//  effifun(...) -> expected detection efficiency for a given threshold 
//
//
// Created by Jerome Baudot, IPHC-Strasbourg, jerome.baudot@iphc.cnrs.fr
//
//-----------------------------------------------------------------------

#include "TH1.h"
#include "TF1.h"
#include "TROOT.h"
#include "TStyle.h"
#include "TMath.h"
#include "TLegend.h"
#include "TPad.h"
#include "TCanvas.h"
#include "TLatex.h"


// ===========================================================================
void bichsel() {

  cout << "Load the library first with .L bichsel.C+" << endl;
  cout << " then use one of the following functions:"
  cout << endl;
  cout << "eloss(thickness, noise, conversion): draw Landau MPV and width evolution with thickness." << endl;
  cout << "effi(thickness, sigfraction, noise, conversion): draw efficiencies with threshold for various hypothesis." << endl;
  cout << endl;
  cout << "Computations based on H. Bichsel Rev. Mod. Phys. , Vol. 60, No. 3, July 1988." << endl;
  cout << "Units: thickness (cm), noise (e-)." << endl;
  cout << endl;

}

// ===========================================================================
Double_t langaufun(Double_t *x, Double_t *par) {
  // Convoluted Landau and Gaussian Fitting Function
  //         (using ROOT's Landau and Gauss functions)
  //
  //  Based on a Fortran code by R.Fruehwirth (fruhwirth@hephy.oeaw.ac.at)
  //  Adapted for C++/ROOT by H.Pernegger (Heinz.Pernegger@cern.ch) and
  //   Markus Friedl (Markus.Friedl@cern.ch)
  //

  //Fit parameters:
  //par[0]=Width (scale) parameter of Landau density
  //par[1]=Most Probable (MP, location) parameter of Landau density
  //par[2]=Total area (integral -inf to inf, normalization constant)
  //par[3]=Width (sigma) of convoluted Gaussian function
  //
  //In the Landau distribution (represented by the CERNLIB approximation),
  //the maximum is located at x=-0.22278298 with the location parameter=0.
  //This shift is corrected within this function, so that the actual
  //maximum is identical to the MP parameter.

  // Numeric constants
  Double_t invsq2pi = 0.3989422804014;   // (2 pi)^(-1/2)
  Double_t mpshift  = -0.22278298;       // Landau maximum location

  // Control constants
  Double_t np = 100.0;      // number of convolution steps
  Double_t sc =   5.0;      // convolution extends to +-sc Gaussian sigmas

  // Variables
  Double_t xx;
  Double_t mpc;
  Double_t fland;
  Double_t sum = 0.0;
  Double_t xlow,xupp;
  Double_t step;
  Double_t i;


  // MP shift correction
  mpc = par[1] - mpshift * par[0];

  // Range of convolution integral
  xlow = x[0] - sc * par[3];
  xupp = x[0] + sc * par[3];

  step = (xupp-xlow) / np;

  // printf(" mpv %.3f, xlow %.3f, xupp %.3f, step %.3f, par0 %.3f, par1 %.3f, par2 %.3f, par3 %.3f\n", mpc, xlow, xupp, step, par[0], par[1], par[2], par[3]);

  // Convolution integral of Landau and Gaussian by sum
  for(i=1.0; i<=np/2; i++) {
    xx = xlow + (i-.5) * step;
    fland = TMath::Landau(xx,mpc,par[0]) / par[0];
    sum += fland * TMath::Gaus(x[0],xx,par[3]);

    xx = xupp - (i-.5) * step;
    fland = TMath::Landau(xx,mpc,par[0]) / par[0];
    sum += fland * TMath::Gaus(x[0],xx,par[3]);
  }

  return (par[2] * step * sum * invsq2pi / par[3]);
}

// ===========================================================================
Double_t mpvfun(Double_t *thickness, Double_t *par) {
  // Return the expected MPV from H. Bichsel Rev. Mod. Phys. , Vol. 60, No. 3, July 1988
  //  thickness in cm and converted to um
  // MPV in arbitrary unit : MPV = Delta_p/3.6/conversion
  // par[0] = conversion factor e-/arbitrary units
  //
  // Delta_p in eV given by Bichsel, with formula depending on thickness
  //  13 < t <  110 um Delta_p = t (100.6 + 35.35 ln(t))
  // 110 < t < 3000 um Delta_p = t (190 + 16.3 ln(t))

  Double_t t = thickness[0]*1e4; // cm -> um
  Double_t Delta_p = 0.;

  if ( 1 <= t && t < 110 ) {
    Delta_p = t * (100.6 + 35.35*log(t));
  }
  else if ( 110 <= t && t <= 3000 ) {
    Delta_p = t * (190 + 16.3*log(t));
  }

  return  Delta_p / 3.6 / par[0];

}

// ===========================================================================
Double_t widthfun(Double_t *thickness, Double_t *par) {
  // Return the expected Landau width from H. Bichsel Rev. Mod. Phys. , Vol. 60, No. 3, July 1988
  // thickness in cm
  // Landau width in arbitrary unit : width = FWHM/4/3.6/conversion
  // par[0] = conversion factor e-/arbitrary units
  //
  // FWHM w in ev given by Bichsel, with formula depending on thickness
  // 11 < t < 30 um
  //    FWHM w = t (147.7 - 2.72 ln(t))
  // 30 < t < 260 um
  //    FWHM w = t (259.6 - 28.4 ln(t))

  Double_t t = thickness[0]*1e4;
  Double_t w = 0.;

  if ( 1 < t && t < 30 ) {
    w = t * (259.6 - 28.4*log(t));
  }
  else if ( 30 <= t && t < 2560 ) {
  //else if ( 30 <= t && t < 260 ) {
    w = t * (259.6 - 28.4*log(t));
  }
  //else if ( 260 <= t && t < 2560 ) {
  //  w = 71.3 * t * (1 + 39.4*pow(t,0.8));
  //}

  return w / 4 / 3.6 / par[0];

}

// ===========================================================================
Double_t ratiofun(Double_t *thickness, Double_t *par) {
  // Return the expected ratio width/MPV from H. Bichsel Rev. Mod. Phys. , Vol. 60, No. 3, July 1988
  //  thickness in cm and converted to um
  // MPV in arbitrary unit : MPV = Delta_p/3.6/conversion
  // par[0] = conversion factor e-/arbitrary units
  //
  // Delta_p in eV given by Bichsel, with formula depending on thickness
  //  13 < t <  110 um Delta_p = t (100.6 + 35.35 ln(t))
  // 110 < t < 3000 um Delta_p = t (190 + 16.3 ln(t))

  Double_t mpv = mpvfun( thickness, par);
  Double_t width = widthfun( thickness, par);

  if ( mpv>0. ) {
    return width/mpv;
  }

  return 0.;

}

// ===========================================================================
Double_t mpvpmicfun(Double_t *thickness, Double_t *par) {
  // Return the expected MPV per micron from H. Bichsel Rev. Mod. Phys. , Vol. 60, No. 3, July 1988
  //  thickness in cm and converted to um
  // MPV in arbitrary unit : MPV = Delta_p/3.6/conversion
  // par[0] = conversion factor e-/arbitrary units
  //
  // Delta_p in eV given by Bichsel, with formula depending on thickness
  //  13 < t <  110 um Delta_p = t (100.6 + 35.35 ln(t))
  // 110 < t < 3000 um Delta_p = t (190 + 16.3 ln(t))

  Double_t t = thickness[0]*1e4; // cm -> um
  Double_t mpv = mpvfun( thickness, par);

  if (t>0. ) {
    return mpv/t;
  }

  return 0.;
}


// ===========================================================================
Double_t effifun(Double_t *threshold, Double_t *par) {
  // Return the efficieny for a given threshold (arbitrary unit) and a fraction of signal
  // par[0] = conversion factor e-/arbitrary units
  // par[1] = noise (e-)
  // par[2] = thickness (cm)
  // par[3] = signal fraction (meaning fraction of signal on the seed pixel)

  Double_t thickness = par[2];
  Double_t mpv = mpvfun( &thickness, par);
  Double_t width = widthfun( &thickness, par);
  Double_t sigma = par[1]/par[0];
  Double_t max = mpv+50*width;
  Double_t eff = 0.;

  TF1 *flangaus = new TF1( "landau", langaufun, 0., mpv+50*width, 4);
  flangaus->SetParameters( width, mpv, 1., sigma);

  // printf( "thickness=%.1e, width=%.2f, mpv=%.2f, sigma=%.2f, max=%.2f, threshold_eff=%.2f\n", thickness*1e4, width, mpv, sigma, max, threshold[0]*par[3]);

  if( threshold[0]*par[3] < max ) {
    eff = flangaus->Integral(threshold[0]/par[3],max) / flangaus->Integral(0.,max);
  }

  flangaus->Delete();

  return eff;

}

// ===========================================================================
void eloss( Double_t thickness=25e-4, Double_t noise=50, Double_t conversion=1. ) {
  // Draw :
  //  MPV evolution with thickness
  //  Width evolution with thickness
  //  energy loss from Landau distribution

  TCanvas *c1 = new TCanvas("c1", "Landau - Bichsel", 800, 800);
  gStyle->SetPadLeftMargin(.15);
  c1->Divide(2,2);

  Double_t min_thickness=5.e-4;
  Double_t max_thickness=55.e-4;

  TF1 *fmpv = new TF1( "fmpv", mpvfun, min_thickness, max_thickness, 1);
  if( fabs(conversion-1.0)<.1 ) {
    fmpv->SetTitle("MPV(e-) versus Thickness(cm); thickness (cm); Landau MPV (e-)");
  } else {
    fmpv->SetTitle("MPV(adcu) versus Thickness(cm); thickness (cm); Landau MPV (adcu)");
  }
  fmpv->SetParameter(0, conversion);

  TF1 *fwidth = new TF1( "fwidth", widthfun, min_thickness, max_thickness, 1);
  if( fabs(conversion-1.0)<.1 ) {
    fwidth->SetTitle("Width(e-) versus Thickness(cm); thickness (cm); Landau width (e-)");
  } else {
    fwidth->SetTitle("Width(adcu) versus Thickness(cm); thickness (cm); Landau width (adcu)");
  }
  fwidth->SetLineColor(4);
  fwidth->SetParameter(0, conversion);

  TF1 *fratio = new TF1( "fratio", ratiofun, min_thickness, max_thickness, 1);
  fratio->SetTitle("Ratio Width/MPV versus Thickness(cm); thickness (cm); Landau Width/MPV");
  fratio->SetParameter(0, conversion);

  TF1 *fmpvpmic = new TF1( "fmpvpmic", mpvpmicfun, min_thickness, max_thickness, 1);
  fmpvpmic->SetTitle("MPV(e-/um) versus Thickness(cm); thickness (cm); MPV (e-/um)");
  fmpvpmic->SetParameter(0, conversion);



  Double_t mpv = mpvfun(&thickness, &conversion);
  Double_t width = widthfun(&thickness, &conversion);
  Double_t sigma = noise/conversion;

  printf("Thickness of silicon: %.1e cm", thickness);
  printf(" Units: 1 adcu = %.1f e-\n", conversion);
  printf(" Landau parameters: mpv = %.2f adcu, width = %.2f adcu\n", mpv, width);
  printf(" Noise parameter: sigma = %.2f adcu\n", sigma);

  TF1 *flangaus = new TF1( "flangaus", langaufun, 0., mpv+15*width, 4);
  if( fabs(conversion-1.0)<.1 ) {
    flangaus->SetTitle(Form("LanGau: noise %.0f e-, thickness %.1e cm; signal (e-)", sigma, thickness));
  } else {
    flangaus->SetTitle(Form("LanGau: noise %.0f adcu thickness %.1e cm; signal (adcu)", sigma, thickness));
  }
  flangaus->SetParNames( "Width", "MPV", "norm", "noise");
  flangaus->SetParameters( width, mpv, 1., sigma);

  c1->cd(1);
  fmpv->Draw();
  fwidth->Draw("same");
  TLegend *legend = new TLegend(0.2,0.65,0.40,0.80);
  legend->AddEntry(fmpv,"MPV","l");
  legend->AddEntry(fwidth,"Width","l");
  legend->Draw();
  gPad->SetGrid(1,1);

  c1->cd(2);
  fmpvpmic->Draw();
  gPad->SetGrid(1,1);

  c1->cd(4);
  flangaus->SetParameters( width, mpv, 1., sigma);
  flangaus->Draw();
  gPad->SetGrid(1,1);

  c1->cd(3);
  fratio->Draw();
  gPad->SetGrid(1,1);

}

// ===========================================================================
void effi( Double_t thickness_def=25e-4, Double_t sigfraction_def=1./8, Double_t noise=50, Double_t conversion=1. ) {
  // Draw :
  //  efficiency in function of threshold for various thicknesses

  const size_t nThickness = 6;
  Double_t thicknessList[nThickness] = { 10.e-4, 20.e-4, 25.e-4, 30.e-4, 40.e-4, 100.e-4}; // cm

  const size_t nFractions = 5;
  Double_t fractionList[nFractions] = {1., 1./2, 1./3, 1./4, 1./8};

  const size_t nThresholds = 5;
  Double_t thresholdList[nThresholds] = {100., 200., 300., 400., 500.}; // e-

  Color_t colorList[10] = {2, 4, 6, 7, 8, 9, 12, 28, 30, 38}; // for thickness
  Style_t markerList[10] = {20, 24, 21, 25, 22, 26, 23, 32, 34, 28}; // for signalFraction

  TCanvas *c2 = new TCanvas("c2", "Efficiency vs threshold", 150, 100, 700, 1200);
  c2->Divide(1,3);
  TF1 *fp;

  TLatex text;
  text.SetTextFont(42);
  text.SetTextSize(0.04);

  // Define basic Landau to study
  Double_t thickness = thickness_def;
  Double_t mpv = mpvfun(&thickness, &conversion);
  Double_t width = widthfun(&thickness, &conversion);
  Double_t sigma = noise/conversion;
  Double_t sigfraction = 1.;

  printf(" Units: 1 adcu = %.1f e-\n", conversion);
  printf(" For thickness %.0f um:\n", thickness*1.e4);
  printf("  Landau parameters: mpv = %.2f adcu, width = %.2f adcu\n", mpv, width);
  printf("  Noise parameter: sigma = %.2f adcu\n", sigma);
  printf("\n");

  TF1 *flangaus = new TF1( "flangaus", langaufun, 0., mpv+10*width, 4);
  if( fabs(conversion-1.0)<.1 ) {
    flangaus->SetTitle("Landau*Gauss distribution; Signal (e-)");
  } else {
    flangaus->SetTitle("Landau*Gauss distribution; Signal (adcu)");
  }
  flangaus->SetParNames( "width", "MPV", "norm", "#sigma");
  flangaus->SetParameters( width, mpv, 1., sigma);
  flangaus->SetLineWidth(3);

  thickness = thicknessList[nThickness-1];
  mpv = mpvfun(&thickness, &conversion);
  width = widthfun(&thickness, &conversion);
  Double_t max_range = mpv*1.2;
  thickness = thicknessList[0];
  mpv = mpvfun(&thickness, &conversion);
  width = widthfun(&thickness, &conversion);
  flangaus->SetParameters( width, mpv, 1., sigma);
  Double_t max_prob = flangaus->Eval(mpv);
  printf(" For display: max_prob=%.1e, max_range=%.0f\n", max_prob, max_range);
  TH1F *hlandau = new TH1F( "hlandau", "Landau distribution; Signal (adcu)", 2000, 0, max_range);
  if( fabs(conversion-1.0)<.1 ) {
    hlandau->SetXTitle("Signal (e-)");
  }
  hlandau->SetStats(0);
  hlandau->SetMaximum(1.2*max_prob);
  hlandau->SetMinimum(1.e-12);

  c2->cd(1);
  hlandau->Draw();
  text.DrawLatexNDC( .15, .80, Form("From Landau fluctuation by Rev.Mod.Phys.60(1988)663 x Gauss(%.0f adcu)",noise/conversion));

  TLegend* legend1 = new TLegend(0.65,0.45,0.85,0.75, "Thickness:");
  legend1->SetFillStyle(1);
  legend1->SetFillColor(0);
  // legend1->SetBorderSize(1.);
  legend1->SetTextSize(.06);

  for (size_t i = 0; i < nThickness; i++) {
    thickness = thicknessList[i];
    mpv = mpvfun(&thickness, &conversion);
    width = widthfun(&thickness, &conversion);
    flangaus->SetRange( 0, max_range);
    flangaus->SetParameters( width, mpv, 1., sigma);
    flangaus->SetLineColor(colorList[i]);
    flangaus->SetNpx(2000);
    printf( "  thick=%.0f um, Prob(0)=%.1e, Prob(mpv=%.0f)=%.1e, Prob(mpv+width=%.0f)=%.1e\n", thickness*1.e4, flangaus->Eval(0.), mpv, flangaus->Eval(mpv), mpv+width, flangaus->Eval(mpv+width));
    fp = (TF1*)flangaus->DrawCopy("same");
    legend1->AddEntry( fp, Form(" %.0f #mum", thickness*1.e4), "l");
  }

  legend1->Draw();


  //==========================================
  // Histo and funcrion needed for efficiency studies

  TH1F * heffi = new TH1F( "heffi", "Efficiency vs threshold; Threshold (adcu); Efficiency %", 100, 0, thresholdList[nThresholds-1]+thresholdList[0]);
  heffi->SetStats(0);
  heffi->SetMaximum(1.1);
  heffi->SetMinimum(0.7);

  TF1 *feffi = new TF1( "feffi", effifun, 0, thresholdList[nThresholds-1]+thresholdList[0], 4);
  feffi->SetTitle("Efficiency vs threshold");
  feffi->SetParameters(conversion, noise, thickness, sigfraction);
  feffi->SetLineWidth(3);


  //==========================================
  // Study on threshold with fixed thickness

  thickness = thickness_def;

  printf("\n==========================\nEfficiencies for thickness %.2f um:\n\n", thickness*1.e4);

  c2->cd(2);
  heffi->Draw();
  text.DrawLatex( thresholdList[0]/2, 1.08, Form("From Landau fluctuation by Rev.Mod.Phys.60(1988)663 x Gauss(%.0f adcu)",noise/conversion));
  gPad->SetGrid(1,1);

  TLegend* legend = new TLegend(0.15,0.15,0.45,0.45, Form("For thickness %.0f #mum",thickness*1e4));
  legend->SetFillStyle(1);
  legend->SetFillColor(0);
  // legend->SetBorderSize(1.);
  legend->SetTextSize(.06);

  feffi->SetMarkerColor(colorList[1]);
  feffi->SetMarkerSize(.5);
  for (size_t i = 0; i < nFractions; i++) {
    sigfraction = fractionList[i];
    feffi->SetParameters(conversion, noise, thickness, sigfraction);
    feffi->SetMarkerStyle(markerList[i]);
    printf("* Efficiencies for signal fraction %.2f:\n", sigfraction);
    for (size_t j = 0; j < nThresholds; j++) {
      printf("  thres=%.0f(adcu) -> eff=%.2f %%\n", thresholdList[j], 100*feffi->Eval(thresholdList[j]));
    }
    fp = (TF1*)feffi->DrawCopy("Psame");
    legend->AddEntry( fp, Form("signal fraction %.2f", sigfraction), "P");
  }

  legend->Draw();


  // Study on thickness with fixed signal fraction

  sigfraction = sigfraction_def;
  printf("\n==========================\nEfficiencies for signal fraction %.2f:\n\n", sigfraction);

  c2->cd(3);
  heffi->Draw();
  text.DrawLatex( thresholdList[0]/2, 1.08, Form("From Landau fluctuation by Rev.Mod.Phys.60(1988)663 x Gauss(%.0f adcu)",noise/conversion));
  gPad->SetGrid(1,1);

  TLegend* legend2 = new TLegend(0.11,0.15,0.40,0.45, Form("For signal fraction %.2f", sigfraction));
  legend2->SetFillStyle(1);
  legend2->SetFillColor(0);
  // legend2->SetBorderSize(1.);
  legend2->SetTextSize(.06);

  for (size_t i = 0; i < nThickness; i++) {
    thickness = thicknessList[i];
    feffi->SetParameters(conversion, noise, thickness, sigfraction);
    feffi->SetLineColor(colorList[i]);
    printf("* Efficiencies for thickness %.0f um:\n", thickness*1.e4);
    for (size_t j = 0; j < nThresholds; j++) {
      printf("  thres=%.0f(adcu) -> eff=%.2f %%\n", thresholdList[j], 100*feffi->Eval(thresholdList[j]));
    }
    fp = (TF1*)feffi->DrawCopy("same");
    legend2->AddEntry( fp, Form("thickness %.0f #mum", thickness*1.e4), "l");
  }

  legend2->Draw();


}	
